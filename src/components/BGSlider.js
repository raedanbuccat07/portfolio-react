import BackgroundSlider from 'react-background-slider'

function BGSlider() {

    function importAll(r) {
      return r.keys().map(r);
    }
  
    const images = importAll(require.context('../images', false, /\.jpg/));
  
    return (
        <BackgroundSlider
          images={[images[0],images[1],images[2]]}
          duration={15} transition={0.5} className="test"
        />
    );
  }
  
  export default BGSlider;
  