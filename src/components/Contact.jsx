import { Row, Col, Container } from 'react-bootstrap'
import React, { Fragment } from 'react'
import { SocialIcon } from 'react-social-icons';

export default function Contact() {


    return (
        <Fragment>
            <Container fluid className="p-0 m-0">
                <Container className="main-container main-bg-color mb-5">
                    <Row>
                        <Col className="col-12">
                            <h3 className="mt-3 ms-3 text-secondary about-line">Contact</h3>
                        </Col>
                        <Col className="col-12">
                            <h1 className="ms-3">CONTACT DETAILS</h1>
                        </Col>
                    </Row>
                    <Row>
                        <Col className="col-lg-6 col-md-12">
                            <Row>
                                <Col className="col-12">
                                    <h4 className="mt-3 ms-3"><span className="full-stack-color">&gt; </span>Email: <span className="span-details">raedanbuccat07@gmail.com</span></h4>
                                </Col>
                                <Col className="col-12">
                                    <h4 className="mt-3 ms-3"><span className="full-stack-color">&gt; </span>Mobile No: <span className="span-details">0921-870-6542</span></h4>
                                </Col>
                                <Col className="col-12">
                                    <h4 className="mt-3 ms-3"><span className="full-stack-color">&gt; </span>Social Networks: <SocialIcon network="linkedin" target='_blank' url="https://www.linkedin.com/in/rae-dan-buccat-ab7121226/" />
                                    </h4>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Container>
            </Container>
        </Fragment>
    )
}