import { Row, Col, Container, Card, CardGroup } from 'react-bootstrap'
import React, { Fragment } from 'react'
import LogoHTML from '../images/tools/html5.png'
import LogoBootstrap from '../images/tools/bootstrap.png'
import LogoCSS from '../images/tools/css.png'
import LogoJavascript from '../images/tools/javascript.png'
import LogoReact from '../images/tools/react.png'
import LogoMongo from '../images/tools/mongodb.png'
import LogoExpress from '../images/tools/expressjs.png'
import LogoNode from '../images/tools/nodejs.png'
import LogoRest from '../images/tools/restAPI.png'
import LogoBash from '../images/tools/gitbash.png'
import LogoGit from '../images/tools/git.png'
import LogoGitLab from '../images/tools/gitlab.png'
import LogoSublime from '../images/tools/sublimetext.png'
import LogoVSCode from '../images/tools/visual.png'
import LogoPostman from '../images/tools/postman.png'
import LogoHeroku from '../images/tools/heroku.png'
import LogoJava from '../images/tools/java.png'
import LogoPHP from '../images/tools/php.png'

export default function Tools() {

    const projectHeight = "15rem"

    return (
        <Fragment>
            <Container fluid className="p-0 m-0 main-bg-color main-container mb-5">
                <Row>
                    <Col className="col-12">
                        <h3 className="mt-3 ms-3 text-secondary about-line">TOOLS</h3>
                    </Col>
                    <Col className="col-12">
                        <h1 className="ms-3">TOOLS THAT I USE</h1>
                    </Col>
                </Row>
                <Row>
                    <Col className="col-12 border-bottom mt-5">
                        <h5 className="ms-3 text-center"><span className="text-color-gold">Programming Tools</span></h5>
                    </Col>
                </Row>
                <Row className="d-flex justify-content-center">
                    <Col className="my-3 mx-2 col-auto p-0 m-0 d-flex justify-content-center">
                        <CardGroup>
                            <Card
                                onClick={() => window.open("https://en.wikipedia.org/wiki/HTML5", '_blank')}
                                className="card-product"
                                style={{ height: projectHeight }}
                            >
                                <Card.Img
                                    className="card-image-style rounded illustration-thumbnail"
                                    variant="top"
                                    src={LogoHTML} />
                                <Card.Body className="p-0 m-0 d-flex justify-content-center">
                                    <Row className="w-100 p-0 m-0">
                                        <Col className="mt-2">
                                            <h5 className="text-white text-center">HTML</h5>
                                        </Col>
                                    </Row>
                                </Card.Body>
                            </Card>
                            <Card
                                onClick={() => window.open("https://en.wikipedia.org/wiki/CSS", '_blank')}
                                className="card-product"
                                style={{ height: projectHeight }}
                            >
                                <Card.Img
                                    className="card-image-style rounded illustration-thumbnail"
                                    variant="top"
                                    src={LogoCSS} />
                                <Card.Body className="p-0 m-0 d-flex justify-content-center">
                                    <Row className="w-100 p-0 m-0">
                                        <Col className="mt-2">
                                            <h5 className="text-white text-center">CSS</h5>
                                        </Col>
                                    </Row>
                                </Card.Body>
                            </Card>
                            <Card
                                onClick={() => window.open("https://www.javascript.com/", '_blank')}
                                className="card-product"
                                style={{ height: projectHeight }}
                            >
                                <Card.Img
                                    className="card-image-style rounded illustration-thumbnail"
                                    variant="top"
                                    src={LogoJavascript} />
                                <Card.Body className="p-0 m-0 d-flex justify-content-center">
                                    <Row className="w-100 p-0 m-0">
                                        <Col className="mt-2">
                                            <h5 className="text-white text-center">Javascript</h5>
                                        </Col>
                                    </Row>
                                </Card.Body>
                            </Card>
                        </CardGroup>
                    </Col>
                </Row>
                <Row className="d-flex justify-content-center">
                    <Col className="my-3 mx-2 col-auto p-0 m-0 d-flex justify-content-center">
                        <CardGroup>
                            <Card
                                onClick={() => window.open("https://www.mongodb.com/", '_blank')}
                                className="card-product"
                                style={{ height: projectHeight }}
                            >
                                <Card.Img
                                    className="card-image-style rounded illustration-thumbnail"
                                    variant="top"
                                    src={LogoMongo} />
                                <Card.Body className="p-0 m-0 d-flex justify-content-center">
                                    <Row className="w-100 p-0 m-0">
                                        <Col className="mt-2">
                                            <h5 className="text-white text-center">MongoDB</h5>
                                        </Col>
                                    </Row>
                                </Card.Body>
                            </Card>
                            <Card
                                onClick={() => window.open("https://expressjs.com/", '_blank')}
                                className="card-product"
                                style={{ height: projectHeight }}
                            >
                                <Card.Img
                                    className="card-image-style rounded illustration-thumbnail"
                                    variant="top"
                                    src={LogoExpress} />
                                <Card.Body className="p-0 m-0 d-flex justify-content-center">
                                    <Row className="w-100 p-0 m-0">
                                        <Col className="mt-2">
                                            <h5 className="text-white text-center">Express JS</h5>
                                        </Col>
                                    </Row>
                                </Card.Body>
                            </Card>
                            <Card
                                onClick={() => window.open("https://reactjs.org/", '_blank')}
                                className="card-product"
                                style={{ height: projectHeight }}
                            >
                                <Card.Img
                                    className="card-image-style rounded illustration-thumbnail"
                                    variant="top"
                                    src={LogoReact} />
                                <Card.Body className="p-0 m-0 d-flex justify-content-center">
                                    <Row className="w-100 p-0 m-0">
                                        <Col className="mt-2">
                                            <h5 className="text-white text-center">React JS</h5>
                                        </Col>
                                    </Row>
                                </Card.Body>
                            </Card>
                            <Card
                                onClick={() => window.open("https://nodejs.org/en/", '_blank')}
                                className="card-product"
                                style={{ height: projectHeight }}
                            >
                                <Card.Img
                                    className="card-image-style rounded illustration-thumbnail"
                                    variant="top"
                                    src={LogoNode} />
                                <Card.Body className="p-0 m-0 d-flex justify-content-center">
                                    <Row className="w-100 p-0 m-0">
                                        <Col className="mt-2">
                                            <h5 className="text-white text-center">Node JS</h5>
                                        </Col>
                                    </Row>
                                </Card.Body>
                            </Card>
                            <Card
                                onClick={() => window.open("https://getbootstrap.com/", '_blank')}
                                className="card-product"
                                style={{ height: projectHeight }}
                            >
                                <Card.Img
                                    className="card-image-style rounded illustration-thumbnail"
                                    variant="top"
                                    src={LogoBootstrap} />
                                <Card.Body className="p-0 m-0 d-flex justify-content-center">
                                    <Row className="w-100 p-0 m-0">
                                        <Col className="mt-2">
                                            <h5 className="text-white text-center">Bootstrap</h5>
                                        </Col>
                                    </Row>
                                </Card.Body>
                            </Card>
                            <Card
                                onClick={() => window.open("https://en.wikipedia.org/wiki/Representational_state_transfer", '_blank')}
                                className="card-product"
                                style={{ height: projectHeight }}
                            >
                                <Card.Img
                                    className="card-image-style rounded illustration-thumbnail"
                                    variant="top"
                                    src={LogoRest} />
                                <Card.Body className="p-0 m-0 d-flex justify-content-center">
                                    <Row className="w-100 p-0 m-0">
                                        <Col className="mt-2">
                                            <h5 className="text-white text-center">REST API</h5>
                                        </Col>
                                    </Row>
                                </Card.Body>
                            </Card>
                        </CardGroup>
                    </Col>
                </Row>
                <Row className="d-flex justify-content-center">
                    <Col className="my-3 mx-2 col-auto p-0 m-0 d-flex justify-content-center">
                        <CardGroup>
                            <Card
                                onClick={() => window.open("https://en.wikipedia.org/wiki/Git", '_blank')}
                                className="card-product"
                                style={{ height: projectHeight }}
                            >
                                <Card.Img
                                    className="card-image-style rounded illustration-thumbnail"
                                    variant="top"
                                    src={LogoBash} />
                                <Card.Body className="p-0 m-0 d-flex justify-content-center">
                                    <Row className="w-100 p-0 m-0">
                                        <Col className="mt-2">
                                            <h5 className="text-white text-center">Git Bash</h5>
                                        </Col>
                                    </Row>
                                </Card.Body>
                            </Card>
                            <Card
                                onClick={() => window.open("https://en.wikipedia.org/wiki/Git", '_blank')}
                                className="card-product"
                                style={{ height: projectHeight }}
                            >
                                <Card.Img
                                    className="card-image-style rounded illustration-thumbnail"
                                    variant="top"
                                    src={LogoGit} />
                                <Card.Body className="p-0 m-0 d-flex justify-content-center">
                                    <Row className="w-100 p-0 m-0">
                                        <Col className="mt-2">
                                            <h5 className="text-white text-center">Git</h5>
                                        </Col>
                                    </Row>
                                </Card.Body>
                            </Card>
                            <Card
                                onClick={() => window.open("https://about.gitlab.com/", '_blank')}
                                className="card-product"
                                style={{ height: projectHeight }}
                            >
                                <Card.Img
                                    className="card-image-style rounded illustration-thumbnail"
                                    variant="top"
                                    src={LogoGitLab} />
                                <Card.Body className="p-0 m-0 d-flex justify-content-center">
                                    <Row className="w-100 p-0 m-0">
                                        <Col className="mt-2">
                                            <h5 className="text-white text-center">Git Lab</h5>
                                        </Col>
                                    </Row>
                                </Card.Body>
                            </Card>
                            <Card
                                onClick={() => window.open("https://www.sublimetext.com/", '_blank')}
                                className="card-product"
                                style={{ height: projectHeight }}
                            >
                                <Card.Img
                                    className="card-image-style rounded illustration-thumbnail"
                                    variant="top"
                                    src={LogoSublime} />
                                <Card.Body className="p-0 m-0 d-flex justify-content-center">
                                    <Row className="w-100 p-0 m-0">
                                        <Col className="mt-2">
                                            <h5 className="text-white text-center">Sublime Text</h5>
                                        </Col>
                                    </Row>
                                </Card.Body>
                            </Card>
                            <Card
                                onClick={() => window.open("https://code.visualstudio.com/", '_blank')}
                                className="card-product"
                                style={{ height: projectHeight }}
                            >
                                <Card.Img
                                    className="card-image-style rounded illustration-thumbnail"
                                    variant="top"
                                    src={LogoVSCode} />
                                <Card.Body className="p-0 m-0 d-flex justify-content-center">
                                    <Row className="w-100 p-0 m-0">
                                        <Col className="mt-2">
                                            <h5 className="text-white text-center">VS Code</h5>
                                        </Col>
                                    </Row>
                                </Card.Body>
                            </Card>
                            <Card
                                onClick={() => window.open("https://www.postman.com/", '_blank')}
                                className="card-product"
                                style={{ height: projectHeight }}
                            >
                                <Card.Img
                                    className="card-image-style rounded illustration-thumbnail"
                                    variant="top"
                                    src={LogoPostman} />
                                <Card.Body className="p-0 m-0 d-flex justify-content-center">
                                    <Row className="w-100 p-0 m-0">
                                        <Col className="mt-2">
                                            <h5 className="text-white text-center">Postman</h5>
                                        </Col>
                                    </Row>
                                </Card.Body>
                            </Card>
                            <Card
                                onClick={() => window.open("https://www.heroku.com", '_blank')}
                                className="card-product"
                                style={{ height: projectHeight }}
                            >
                                <Card.Img
                                    className="card-image-style rounded illustration-thumbnail"
                                    variant="top"
                                    src={LogoHeroku} />
                                <Card.Body className="p-0 m-0 d-flex justify-content-center">
                                    <Row className="w-100 p-0 m-0">
                                        <Col className="mt-2">
                                            <h5 className="text-white text-center">Heroku</h5>
                                        </Col>
                                    </Row>
                                </Card.Body>
                            </Card>
                        </CardGroup>
                    </Col>
                </Row>
                <Row className="d-flex justify-content-center pb-5">
                    <Col className="my-3 mx-2 col-auto p-0 m-0 d-flex justify-content-center">
                        <CardGroup>
                            <Card
                                onClick={() => window.open("https://www.php.net/", '_blank')}
                                className="card-product"
                                style={{ height: projectHeight }}
                            >
                                <Card.Img
                                    className="card-image-style rounded illustration-thumbnail"
                                    variant="top"
                                    src={LogoPHP} />
                                <Card.Body className="p-0 m-0 d-flex justify-content-center">
                                    <Row className="w-100 p-0 m-0">
                                        <Col className="mt-2">
                                            <h5 className="text-white text-center">PHP</h5>
                                        </Col>
                                    </Row>
                                </Card.Body>
                            </Card>
                            <Card
                                onClick={() => window.open("https://www.java.com/en/", '_blank')}
                                className="card-product"
                                style={{ height: projectHeight }}
                            >
                                <Card.Img
                                    className="card-image-style rounded illustration-thumbnail"
                                    variant="top"
                                    src={LogoJava} />
                                <Card.Body className="p-0 m-0 d-flex justify-content-center">
                                    <Row className="w-100 p-0 m-0">
                                        <Col className="mt-2">
                                            <h5 className="text-white text-center">Java</h5>
                                        </Col>
                                    </Row>
                                </Card.Body>
                            </Card>
                        </CardGroup>
                    </Col>
                </Row>
            </Container>
        </Fragment>
    )
}