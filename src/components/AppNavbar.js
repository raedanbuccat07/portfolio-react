import { Navbar, Nav, Container } from 'react-bootstrap'
import React, { Fragment } from 'react'

export default function AppNavbar() {

    return (
        <Fragment>
            <Navbar expand="lg" className="w-100 border-bottom fixed-top">
                <Container className="h-100 w-100">
                    <Navbar.Brand className="text-white" href="#Home">Rae Dan<small>Buccat</small></Navbar.Brand>
                    <Navbar.Toggle className="text-white" aria-controls="basic-navbar-nav">Menu</Navbar.Toggle>
                    <Navbar.Toggle className="d-none" aria-controls="navbar-dark-example" hidden />
                    <Navbar.Collapse className="justify-content-lg-end justify-content-md-center m-0 p-0 navbar-collapse-background">
                        <Nav className="d-flex justify-content-lg-end justify-content-md-center m-0 p-0 h-100">
                            <Nav.Link className="mt-3 justify-content-md-center" href="#Home">Home</Nav.Link>
                            <Nav.Link className="mt-3 justify-content-md-center" href="#About">About Me</Nav.Link>
                            <Nav.Link className="mt-3 justify-content-md-center" href="#Resume">Resume</Nav.Link>
                            <Nav.Link className="mt-3 justify-content-md-center" href="#Projects">Projects</Nav.Link>
                            <Nav.Link className="mt-3 justify-content-md-center" href="#Tools">Tools</Nav.Link>
                            <Nav.Link className="mt-3 justify-content-md-center" href="#Contact">Contact</Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </Fragment>
    )
}