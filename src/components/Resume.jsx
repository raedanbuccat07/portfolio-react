import { Row, Col, Container, Card, Modal, CloseButton, Image, Button } from 'react-bootstrap'
import React, { Fragment, useState } from 'react'
import ZuittCompletion from '../images/certificates/Zuitt.jpeg'
import BestCapstone2 from '../images/certificates/Capstone2.jpg'
import MFI from '../images/certificates/MFI.png'
import DeanLister from '../images/certificates/Dean.png'
import ResumePDF from '../files/Resume-Rae-Dan-Buccat.pdf'

function onResumeClick() {
    window.open(ResumePDF, '_blank');
}

export default function Resume() {

    const [selectedCard, setSelectedCard] = useState()
    const [selectedCardTitle, setSelectedCardTitle] = useState()
    const [show, setShow] = useState(false);

    function handleShow(selected, title) {
        setSelectedCard(selected)
        setSelectedCardTitle(title)
        setShow(true);
    }

    return (
        <Fragment>
            <Modal
                show={show}
                fullscreen={true}
                onHide={() => setShow(false)}
                className="resume-modal"
            >
                <Modal.Header className="resume-modal">
                    <Modal.Title className="text-center w-100">{selectedCardTitle}</Modal.Title>

                    <CloseButton variant="white" onClick={() => setShow(false)} />
                </Modal.Header>
                <Modal.Body className="resume-modal d-flex justify-content-center">
                    <Container fluid className="d-flex justify-content-center align-items-center">
                        <Container className="d-flex justify-content-center align-items-center">
                            <Image className="selected-resume-card" src={selectedCard} />
                        </Container>
                    </Container>
                </Modal.Body>
            </Modal>
            <Container fluid className="p-0 m-0">
                <Container className="main-container main-bg-color mb-5">
                    <Row>
                        <Col className="col-12">
                            <h3 className="mt-3 ms-3 text-secondary about-line">RESUME</h3>
                        </Col>
                        <Col className="col-12">
                            <h1 className="ms-3">CHECK MY RESUME | <Button onClick={() => onResumeClick()}>download PDF version resume</Button></h1>
                        </Col>
                        <Col className="col-12">
                            <h2 className="mt-3 ms-3 full-stack-color">Rae Dan Emmanuel T. Buccat - Full-Stack Web Developer</h2>
                        </Col>
                        <Col className="col-12">
                            <Row>
                                <Col className="col-12">
                                    <h2 className="mt-3 ms-3"><span className="full-stack-color">&gt; </span>Personal &amp; Contact Information<span className="span-details"></span></h2>
                                </Col>
                                <Col className="col-12 col-lg-6 summary-border">
                                    <Row>
                                        <Col className="col-12">
                                            <h4 className="mt-3 ms-3">Birthday: <span className="span-details">07, December, 1996</span></h4>
                                        </Col>
                                        <Col className="col-12">
                                            <h4 className="mt-3 ms-3">Civil Status: <span className="span-details">Single</span></h4>
                                        </Col>
                                        <Col className="col-12">
                                            <h4 className="mt-3 ms-3">Nationality: <span className="span-details">Filipino</span></h4>
                                        </Col>
                                    </Row>
                                </Col>
                                <Col className="col-12 col-lg-6 summary-border">
                                    <Row>
                                        <Col className="col-12">
                                            <h4 className="mt-3 ms-3">Mobile: <span className="span-details">0921-870-6542</span></h4>
                                        </Col>
                                        <Col className="col-12">
                                            <h4 className="mt-3 ms-3">Email: <span className="span-details">raedanbuccat07@gmail.com</span></h4>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                        <Col className="col-12 col-lg-6">
                            <Row>
                                <Col className="col-12">
                                    <h2 className="mt-3 ms-3"><span className="full-stack-color">&gt; </span>Summary<span className="span-details"></span></h2>
                                </Col>
                                <Col className="col-12 summary-border">
                                    <h4 className="mt-3 ms-3">
                                        <span className="span-details">
                                            -I am a Bachelor of Information Technology graduate from Far Eastern University Makati.
                                        </span>
                                    </h4>
                                    <h4 className="mt-3 ms-3">
                                        <span className="span-details">
                                            -I aim to express my professionalism in terms of web developing and to improve my skills further as my career growth progresses in my field of expertise.
                                        </span>
                                    </h4>
                                </Col>
                            </Row>
                            <Row>
                                <Col className="col-12">
                                    <h2 className="mt-3 ms-3"><span className="full-stack-color">&gt; </span>Experience<span className="span-details"></span></h2>
                                </Col>
                                <Col className="col-12">
                                    <h5 className="mt-3 ms-3 border-bottom">
                                        <span className="span-details">
                                            <span className="full-stack-color">Zuitt - Coding Bootcamp</span> (Nov 2021 - Jan 2022)
                                        </span>
                                    </h5>
                                </Col>
                                <Col className="col-12 indra-border">
                                    <h6 className="mt-3 ms-3">
                                        <span className="span-details">
                                            Position: Student
                                        </span>
                                    </h6>
                                    <h6 className="mt-3 ms-3">
                                        <span className="span-details border-bottom">
                                            - <span className="full-stack-color">Capstone Project 3</span> - E-commerce Application Full-Stack
                                        </span>
                                    </h6>
                                    <h6 className="mt-3 ms-3">
                                        <span className="span-details">
                                            ・Developed an E-commerce App using MERN Stack
                                        </span>
                                    </h6>
                                    <h6 className="mt-3 ms-3">
                                        <span className="span-details">
                                            ・MongoDB, Express.js, React.js, Node.js
                                        </span>
                                    </h6>

                                    <h6 className="mt-3 ms-3">
                                        <span className="span-details border-bottom">
                                            - <span className="full-stack-color">Capstone Project 2</span> - E-commerce Application (Back-End Only)
                                        </span>
                                    </h6>
                                    <h6 className="mt-3 ms-3">
                                        <span className="span-details">
                                            ・Developed an App and learned how server works, manage its data, connection to online database,
                                            and online deployment
                                        </span>
                                    </h6>
                                    <h6 className="mt-3 ms-3">
                                        <span className="span-details">
                                            ・Used technologies: JavaScript, MongoDB, Node.js, Express.js, Postman, REST API and Heroku
                                        </span>
                                    </h6>

                                    <h6 className="mt-3 ms-3">
                                        <span className="span-details border-bottom">
                                            - <span className="full-stack-color">Capstone Project 1</span> - Portfolio Website
                                        </span>
                                    </h6>
                                    <h6 className="mt-3 ms-3">
                                        <span className="span-details">
                                            ・Created a static website showcasing a Developer Portfolio
                                        </span>
                                    </h6>
                                    <h6 className="mt-3 ms-3">
                                        <span className="span-details">
                                            ・Used REACT JS, Bootstrap, CSS, HTML and JavaScript to build the website; and hosting it using
                                            Vercel
                                        </span>
                                    </h6>
                                </Col>
                                <Col className="col-12">
                                    <h5 className="mt-3 ms-3 border-bottom">
                                        <span className="span-details">
                                            <span className="full-stack-color">Indra Philippines, Inc.</span> (Sep 2018 – Jan 2019)
                                        </span>
                                    </h5>
                                </Col>
                                <Col className="col-12 indra-border">
                                    <h6 className="mt-3 ms-3">
                                        <span className="span-details">
                                            Position: Trainee
                                        </span>
                                    </h6>
                                    <h6 className="mt-3 ms-3">
                                        <span className="span-details">
                                            Specialization:
                                        </span>
                                    </h6>
                                    <h6 className="mt-3 ms-3">
                                        <span className="span-details">
                                            - I.T Support role - to fix bugs on CMS Meralco application using JAVA
                                        </span>
                                    </h6>
                                    <h6 className="mt-3 ms-3">
                                        <span className="span-details">
                                            - Created a CMS website helper (Bootstrap, JavaScript, PHP, HTML, MySQL)
                                        </span>
                                    </h6>
                                    <h6 className="mt-3 ms-3">
                                        <span className="span-details">
                                            Industry: Computer / Information Technology (Software)
                                        </span>
                                    </h6>
                                </Col>
                            </Row>
                        </Col>
                        <Col className="mb-5 col-12 col-lg-6">
                            <Row>
                                <Col className="col-12">
                                    <h2 className="mt-3 ms-3"><span className="full-stack-color">&gt; </span>Education<span className="span-details"></span></h2>
                                </Col>
                                <Col className="col-12">
                                    <h5 className="mt-3 ms-3 border-bottom">
                                        <span className="full-stack-color">Zuitt - Coding Bootcamp</span> (Nov 2021 – Jan 2022) - Latest
                                    </h5>
                                </Col>
                                <Col className="col-12 zuitt-border">
                                    <h4 className="mt-3 ms-3">
                                        <span className="span-details">
                                            - A Web Development Bootcamp focusing on both Front-End and Back-End technologies such as Git,
                                            HTML, CSS, Bootstrap, ReactJS, JavaScript, MongoDB, Express.JS, Node.JS, Postman, and Heroku.

                                        </span>
                                    </h4>
                                </Col>
                                <Col className="col-12">
                                    <h5 className="mt-3 ms-3 border-bottom">
                                        <span className="full-stack-color">Far Eastern University - Bachelor's , Information Technology</span> (2013 – 2019)
                                    </h5>
                                </Col>
                                <Col className="col-12 zuitt-border">
                                    <h4 className="mt-3 ms-3">
                                        <span className="span-details">
                                            - Learned the basics of different programming languages (PHP, Java, Javascript, Bootstrap, Jquery, C++, C#, HTML, CSS, MySQL)
                                        </span>
                                    </h4>
                                </Col>
                            </Row>
                            <Row>
                                <Col className="col-12">
                                    <h2 className="mt-3 ms-3"><span className="full-stack-color">&gt; </span>Skills<span className="span-details"></span></h2>
                                </Col>
                                <Col className="col-12">
                                    <h5 className="mt-3 ms-3 border-bottom">
                                        <span className="full-stack-color">Programming Essentials</span>
                                    </h5>
                                </Col>
                                <Col className="col-12 zuitt-border">
                                    <Row>
                                        <Col className="col-auto">
                                            <h4 className="mt-3 ms-3">
                                                <span className="span-details">
                                                    - Git
                                                </span>
                                            </h4>
                                        </Col>
                                        <Col className="col-auto">
                                            <h4 className="mt-3 ms-3">
                                                <span className="span-details">
                                                    - GitLab
                                                </span>
                                            </h4>
                                        </Col>
                                        <Col className="col-auto">
                                            <h4 className="mt-3 ms-3">
                                                <span className="span-details">
                                                    - Bash
                                                </span>
                                            </h4>
                                        </Col>
                                        <Col className="col-auto">
                                            <h4 className="mt-3 ms-3">
                                                <span className="span-details">
                                                    - Sublime Text
                                                </span>
                                            </h4>
                                        </Col>
                                        <Col className="col-auto">
                                            <h4 className="mt-3 ms-3">
                                                <span className="span-details">
                                                    - Visual Studio Code
                                                </span>
                                            </h4>
                                        </Col>
                                    </Row>
                                </Col>
                                <Col className="col-12">
                                    <h5 className="mt-3 ms-3 border-bottom">
                                        <span className="full-stack-color">Front-end Development</span>
                                    </h5>
                                </Col>
                                <Col className="col-12 zuitt-border">
                                    <Row>
                                        <Col className="col-auto">
                                            <h4 className="mt-3 ms-3">
                                                <span className="span-details">
                                                    - HTML 5
                                                </span>
                                            </h4>
                                        </Col>
                                        <Col className="col-auto">
                                            <h4 className="mt-3 ms-3">
                                                <span className="span-details">
                                                    - CSS 3
                                                </span>
                                            </h4>
                                        </Col>
                                        <Col className="col-auto">
                                            <h4 className="mt-3 ms-3">
                                                <span className="span-details">
                                                    - Bootstrap
                                                </span>
                                            </h4>
                                        </Col>
                                    </Row>
                                </Col>
                                <Col className="col-12">
                                    <h5 className="mt-3 ms-3 border-bottom">
                                        <span className="full-stack-color">Back-end Development</span>
                                    </h5>
                                </Col>
                                <Col className="col-12 zuitt-border">
                                    <Row>
                                        <Col className="col-auto">
                                            <h4 className="mt-3 ms-3">
                                                <span className="span-details">
                                                    - Javascript
                                                </span>
                                            </h4>
                                        </Col>
                                        <Col className="col-auto">
                                            <h4 className="mt-3 ms-3">
                                                <span className="span-details">
                                                    - MongoDB
                                                </span>
                                            </h4>
                                        </Col>
                                        <Col className="col-auto">
                                            <h4 className="mt-3 ms-3">
                                                <span className="span-details">
                                                    - Node.js
                                                </span>
                                            </h4>
                                        </Col>
                                        <Col className="col-auto">
                                            <h4 className="mt-3 ms-3">
                                                <span className="span-details">
                                                    - REST Architecture
                                                </span>
                                            </h4>
                                        </Col>
                                        <Col className="col-auto">
                                            <h4 className="mt-3 ms-3">
                                                <span className="span-details">
                                                    - Express.js
                                                </span>
                                            </h4>
                                        </Col>
                                        <Col className="col-auto">
                                            <h4 className="mt-3 ms-3">
                                                <span className="span-details">
                                                    - Postman
                                                </span>
                                            </h4>
                                        </Col>
                                        <Col className="col-auto">
                                            <h4 className="mt-3 ms-3">
                                                <span className="span-details">
                                                    - Heroku
                                                </span>
                                            </h4>
                                        </Col>
                                        <Col className="col-auto">
                                            <h4 className="mt-3 ms-3">
                                                <span className="span-details">
                                                    - PHP
                                                </span>
                                            </h4>
                                        </Col>
                                        <Col className="col-auto">
                                            <h4 className="mt-3 ms-3">
                                                <span className="span-details">
                                                    - Java
                                                </span>
                                            </h4>
                                        </Col>
                                    </Row>
                                </Col>
                                <Col className="col-12">
                                    <h5 className="mt-3 ms-3 border-bottom">
                                        <span className="full-stack-color">Main Framework</span>
                                    </h5>
                                </Col>
                                <Col className="col-12 zuitt-border">
                                    <h4 className="mt-3 ms-3">
                                        <span className="span-details">
                                            - REACT JS
                                        </span>
                                    </h4>
                                </Col>
                            </Row>
                        </Col>
                        <Col className="col-12">
                            <Row className="mb-5">
                                <Col className="col-12">
                                    <h2 className="mt-3 ms-3"><span className="full-stack-color">&gt; </span>Certifications<span className="span-details"></span></h2>
                                </Col>
                                <Col className="col-12 col-lg-6 indra-border">
                                    <Row>
                                        <Col className="col-12">
                                            <h5 className="mt-3 ms-3 border-bottom">
                                                <span className="span-details">
                                                    <span className="full-stack-color">Career Entry Course for Software Developers - JAVA - Meralco Foundation, Inc. (Aug 2016 - Sep 2016)</span>
                                                </span>
                                            </h5>
                                        </Col>
                                        <Col className="col-12  d-flex justify-content-center">
                                            <Card className="resume-cards" style={{ width: '18rem' }} onClick={() => handleShow(MFI, "Career Entry Course for Software Developers - JAVA - Meralco Foundation, Inc.")}>
                                                <Card.Img variant="top" src={MFI} />
                                            </Card>
                                        </Col>
                                    </Row>
                                </Col>
                                <Col className="col-12 col-lg-6 indra-border">
                                    <Row>
                                        <Col className="col-12">
                                            <h5 className="mt-3 ms-3 border-bottom">
                                                <span className="span-details">
                                                    <span className="full-stack-color">Zuitt, Coding Bootcamp - Certificate of Completion (Nov 2021 - Jan 2022)</span>
                                                </span>
                                            </h5>
                                        </Col>
                                        <Col className="col-12  d-flex justify-content-center">
                                            <Card className="resume-cards" style={{ width: '18rem' }} onClick={() => handleShow(ZuittCompletion, "Zuitt, Coding Bootcamp - Certificate of Completion")}>
                                                <Card.Img variant="top" src={ZuittCompletion} />
                                            </Card>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                            <Row className="mb-5">
                                <Col className="col-12">
                                    <h2 className="mt-3 ms-3"><span className="full-stack-color">&gt; </span>Honor and Awards<span className="span-details"></span></h2>
                                </Col>
                                <Col className="col-12 col-lg-6 indra-border">
                                    <Row>
                                        <Col className="col-12">
                                            <h5 className="mt-3 ms-3 border-bottom">
                                                <span className="span-details">
                                                    <span className="full-stack-color">Far Eastern University - Dean's Lister: Second Honors (Yr. 2018 - 2019)</span>
                                                </span>
                                            </h5>
                                        </Col>
                                        <Col className="col-12  d-flex justify-content-center">
                                            <Card className="resume-cards" style={{ width: '18rem' }} onClick={() => handleShow(DeanLister, "Far Eastern University - Dean's Lister: Second Honors")}>
                                                <Card.Img variant="top" src={DeanLister} />
                                            </Card>
                                        </Col>
                                    </Row>
                                </Col>
                                <Col className="col-12 col-lg-6 indra-border">
                                    <Row>
                                        <Col className="col-12">
                                            <h5 className="mt-3 ms-3 border-bottom">
                                                <span className="span-details">
                                                    <span className="full-stack-color">Zuitt, Coding Bootcamp - Best Capstone 2 (Jan 2022)</span>
                                                </span>
                                            </h5>
                                        </Col>
                                        <Col className="col-12  d-flex justify-content-center">
                                            <Card className="resume-cards" style={{ width: '18rem' }} onClick={() => handleShow(BestCapstone2, "Zuitt, Coding Bootcamp - Best Capstone 2")}>
                                                <Card.Img variant="top" src={BestCapstone2} />
                                            </Card>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Container>
            </Container >
        </Fragment >
    )
}