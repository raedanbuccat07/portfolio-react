import { Row, Col, Container } from 'react-bootstrap'
import React, { Fragment } from 'react'


export default function Home() {


    return (
        <Fragment>
            <Container fluid className="d-flex align-items-center p-0 m-0 home-outside-container">
                <Container className="home-container d-flex justify-content-center align-items-center">
                    <Row className="w-100 p-0 m-0 welcome-box">
                        <Col className="col-12 p-0 m-0 d-flex justify-content-center align-items-center">

                            <h1 className="text-white mt-3">Hello! Welcome to my portfolio.</h1>

                        </Col>
                        <Col className="col-12 p-0 m-0 d-flex justify-content-center align-items-center">
                            <h1 className="mb-4 text-white text-center">You can scroll down to know more about me :)</h1>
                        </Col>
                        <Col className="col-12 p-0 m-0 d-flex justify-content-center align-items-center">
                            <small className="mb-4 mb-md-5 text-white text-center">This portfolio is made with REACT JS</small>
                        </Col>
                    </Row>
                </Container>
            </Container>
        </Fragment>
    )
}