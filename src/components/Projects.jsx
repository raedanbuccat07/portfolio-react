import { Row, Col, Container, Card, Modal, CloseButton, Image } from 'react-bootstrap'
import React, { Fragment, useState } from 'react'
import TimeTracker from '../images/projects/timetracker.png'
import VideoRental from '../images/projects/videorental.png'
import Portfolio from '../images/projects/portfolio.png'
import EminaMiseLogin from '../images/projects/EminaMiseLogin.jpg'
import EminaMiseHome from '../images/projects/EminaMiseHome.jpg'
import EminaMiseIllustrations from '../images/projects/EminaMiseIllustrations.jpg'
import EminaMiseViewIllustration from '../images/projects/EminaMiseViewIllustration.jpg'
import EminaMiseViewUser from '../images/projects/EminaMiseViewUser.jpg'
import EminaMiseCheckout from '../images/projects/EminaMiseCheckout.jpg'

export default function Projects() {

    const [selectedCard, setSelectedCard] = useState()
    const [selectedCardTitle, setSelectedCardTitle] = useState()
    const [show, setShow] = useState(false);

    function handleShow(selected, title) {
        setSelectedCard(selected)
        setSelectedCardTitle(title)
        setShow(true);
    }

    const projectHeight = "20rem"

    return (
        <Fragment>
            <Container fluid className="p-0 m-0 main-bg-color main-container">
                <Row>
                    <Col className="col-12">
                        <h3 className="mt-3 ms-3 text-secondary about-line">PROJECTS</h3>
                    </Col>
                    <Col className="col-12">
                        <h1 className="ms-3">PROJECTS THAT I HAVE MADE</h1>
                    </Col>
                </Row>
                <Row>
                    <Col className="col-12 border-bottom mt-5">
                        <h5
                            style={{cursor:"pointer"}}
                            className="ms-3 text-center"
                            onClick={() => window.open("https://ecommerce-app-self.vercel.app/", '_blank')}>
                            <span className="text-color-gold">EMINA MISE (click here for preview)</span> - E-COMMERCE APP (MERN-STACK, MongoDB, Express JS, REACT JS, Node JS)
                        </h5>

                    </Col>
                </Row>
                <Row className="d-flex justify-content-center">
                    <Col className="my-3 mx-2 col-auto p-0 m-0 d-flex justify-content-center">
                        <Card
                            className="card-product"
                            style={{ height: projectHeight }}
                            onClick={() => handleShow(EminaMiseLogin, "Emina Mise - Login Page")}
                        >
                            <Card.Img
                                className="card-image-style rounded illustration-thumbnail"
                                variant="top"
                                src={EminaMiseLogin} />
                            <Card.Body className="p-0 m-0 d-flex justify-content-center">
                                <Row className="w-100 p-0 m-0">
                                    <Col className="mt-2">
                                        <h5 className="text-white text-center">Login Page</h5>
                                    </Col>
                                </Row>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col className="my-3 mx-2 col-auto p-0 m-0 d-flex justify-content-center">
                        <Card
                            className="card-product"
                            style={{ height: projectHeight }}
                            onClick={() => handleShow(EminaMiseHome, "Emina Mise - Home Page")}
                        >
                            <Card.Img
                                className="card-image-style rounded illustration-thumbnail"
                                variant="top"
                                src={EminaMiseHome} />
                            <Card.Body className="p-0 m-0 d-flex justify-content-center">
                                <Row className="w-100 p-0 m-0">
                                    <Col className="mt-2">
                                        <h5 className="text-white text-center">Home Page</h5>
                                    </Col>
                                </Row>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col className="my-3 mx-2 col-auto p-0 m-0 d-flex justify-content-center">
                        <Card
                            className="card-product"
                            style={{ height: projectHeight }}
                            onClick={() => handleShow(EminaMiseIllustrations, "Emina Mise - Illustrations Page")}
                        >
                            <Card.Img
                                className="card-image-style rounded illustration-thumbnail"
                                variant="top"
                                src={EminaMiseIllustrations} />
                            <Card.Body className="p-0 m-0 d-flex justify-content-center">
                                <Row className="w-100 p-0 m-0">
                                    <Col className="mt-2">
                                        <h5 className="text-white text-center">Illustrations Page</h5>
                                    </Col>
                                </Row>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col className="my-3 mx-2 col-auto p-0 m-0 d-flex justify-content-center">
                        <Card
                            className="card-product"
                            style={{ height: projectHeight }}
                            onClick={() => handleShow(EminaMiseViewIllustration, "Emina Mise - View Illustration Page")}
                        >
                            <Card.Img
                                className="card-image-style rounded illustration-thumbnail"
                                variant="top"
                                src={EminaMiseViewIllustration} />
                            <Card.Body className="p-0 m-0 d-flex justify-content-center">
                                <Row className="w-100 p-0 m-0">
                                    <Col className="mt-2">
                                        <h5 className="text-white text-center">View Illustration Page</h5>
                                    </Col>
                                </Row>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col className="my-3 mx-2 col-auto p-0 m-0 d-flex justify-content-center">
                        <Card
                            className="card-product"
                            style={{ height: projectHeight }}
                            onClick={() => handleShow(EminaMiseViewUser, "Emina Mise - View User Page")}
                        >
                            <Card.Img
                                className="card-image-style rounded illustration-thumbnail"
                                variant="top"
                                src={EminaMiseViewUser} />
                            <Card.Body className="p-0 m-0 d-flex justify-content-center">
                                <Row className="w-100 p-0 m-0">
                                    <Col className="mt-2">
                                        <h5 className="text-white text-center">View User Page</h5>
                                    </Col>
                                </Row>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col className="my-3 mx-2 col-auto p-0 m-0 d-flex justify-content-center">
                        <Card
                            className="card-product"
                            style={{ height: projectHeight }}
                            onClick={() => handleShow(EminaMiseCheckout, "Emina Mise - Checkout Page")}
                        >
                            <Card.Img
                                className="card-image-style rounded illustration-thumbnail"
                                variant="top"
                                src={EminaMiseCheckout} />
                            <Card.Body className="p-0 m-0 d-flex justify-content-center">
                                <Row className="w-100 p-0 m-0">
                                    <Col className="mt-2">
                                        <h5 className="text-white text-center">Checkout Page</h5>
                                    </Col>
                                </Row>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
                <Row>
                    <Col className="col-12 border-bottom mt-5">
                        <h5 className="ms-3 text-center">OTHER PROJECTS</h5>
                    </Col>
                </Row>
                <Row className="d-flex justify-content-center pb-5">
                    <Col className="my-3 mx-2 col-auto p-0 m-0 d-flex justify-content-center">
                        <Card
                            className="card-product"
                            style={{ height: "35rem" }}
                            onClick={() => handleShow(TimeTracker, "Time Tracker")}
                        >
                            <Card.Img
                                className="card-image-style rounded illustration-thumbnail"
                                variant="top"
                                src={TimeTracker} />
                            <Card.Body className="p-0 m-0 d-flex justify-content-center">
                                <Row className="w-100 p-0 m-0">
                                    <Col className="mt-5">
                                        <h5 className="text-white text-center">Time Tracker (PHP, Javascript, Jquery, Bootstrap, HTML, CSS, MySQL)</h5>
                                    </Col>
                                </Row>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col className="my-3 mx-2 col-auto p-0 m-0 d-flex justify-content-center">
                        <Card
                            className="card-product"
                            style={{ height: "35rem" }}
                            onClick={() => handleShow(VideoRental, "Video Rental System")}
                        >
                            <Card.Img
                                className="card-image-style rounded illustration-thumbnail"
                                variant="top"
                                src={VideoRental} />
                            <Card.Body className="p-0 m-0 d-flex justify-content-center">
                                <Row className="w-100 p-0 m-0">
                                    <Col className="mt-5">
                                        <h5 className="text-white text-center">Video Rental System (Java, Javascript, Jquery, HTML, CSS, MySQL)</h5>
                                    </Col>
                                </Row>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col className="my-3 mx-2 col-auto p-0 m-0 d-flex justify-content-center">
                        <Card
                            className="card-product"
                            style={{ height: "35rem" }}
                            onClick={() => handleShow(Portfolio, "My REACT JS portfolio")}
                        >
                            <Card.Img
                                className="card-image-style rounded illustration-thumbnail"
                                variant="top"
                                src={Portfolio} />
                            <Card.Body className="p-0 m-0 d-flex justify-content-center">
                                <Row className="w-100 p-0 m-0">
                                    <Col className="mt-5">
                                        <h5 className="text-white text-center">Personal Portfolio (REACT JS, Javascript, HTML, CSS, Bootstrap)</h5>
                                    </Col>
                                </Row>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Container>

            <Modal
                show={show}
                fullscreen={true}
                onHide={() => setShow(false)}
                className="resume-modal"
            >
                <Modal.Header className="resume-modal">
                    <Modal.Title className="text-center w-100">{selectedCardTitle}</Modal.Title>
                    <CloseButton variant="white" onClick={() => setShow(false)} />
                </Modal.Header>
                <Modal.Body className="resume-modal d-flex justify-content-center">
                    <Container fluid className="d-flex justify-content-center align-items-center">
                        <Container className="d-flex justify-content-center align-items-center">
                            <Image className="selected-resume-card" src={selectedCard} />
                        </Container>
                    </Container>
                </Modal.Body>
            </Modal>
        </Fragment>
    )
}