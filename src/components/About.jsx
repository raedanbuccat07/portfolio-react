import { Row, Col, Container, Card } from 'react-bootstrap'
import React, { Fragment } from 'react'
import Selfie from '../images/RaeDan.png'

export default function About() {


    return (
        <Fragment>
            <Container fluid className="p-0 m-0">
                <Container className="main-container main-bg-color mb-5">
                    <Row>
                        <Col className="col-12">
                            <h3 className="mt-3 ms-3 text-secondary about-line">ABOUT</h3>
                        </Col>
                        <Col className="col-12">
                            <h1 className="ms-3">LEARN MORE ABOUT ME</h1>
                        </Col>
                    </Row>
                    <Row>
                        <Col className="col-lg-4 col-md-12">
                            <Row>
                                <Col className="col-12 d-flex justify-content-center">
                                    <Card className="rounded user-card" style={{ width: "20rem" }}>
                                        <Card.Img variant="top" src={Selfie} className="card-image-style-user illustration-thumbnail" />
                                    </Card>
                                </Col>
                                <Col className="col-12">
                                    <h4 className="ms-3 text-center">Rae Dan Emmanuel T. Buccat</h4>
                                </Col>
                            </Row>
                        </Col>
                        <Col className="col-lg-8 col-md-12">
                            <Row>
                                <Col className="col-12">
                                    <h4 className="mt-3 ms-3 full-stack-color">Full-Stack Web Developer</h4>
                                </Col>
                                <Col className="col-lg-6 col-md-12">
                                    <Row>
                                        <Col className="col-12">
                                            <h4 className="mt-3 ms-3"><span className="full-stack-color">&gt; </span>Birthday: <span className="span-details">07, December, 1996</span></h4>
                                        </Col>
                                        <Col className="col-12">
                                            <h4 className="mt-3 ms-3"><span className="full-stack-color">&gt; </span>Hobby: <span className="span-details">Digital Arts, Programming</span></h4>
                                        </Col>
                                        <Col className="col-12">
                                            <h4 className="mt-3 ms-3"><span className="full-stack-color">&gt; </span>City: <span className="span-details">Marikina City</span></h4>
                                        </Col>
                                    </Row>
                                </Col>
                                <Col className="col-lg-6 col-md-12">
                                    <Row>
                                        <Col className="col-12">
                                            <h4 className="mt-3 ms-3"><span className="full-stack-color">&gt; </span>Email: <span className="span-details">raedanbuccat07@gmail.com</span></h4>
                                        </Col>
                                        <Col className="col-12">
                                            <h4 className="mt-3 ms-3"><span className="full-stack-color">&gt; </span>Alma Mater: <span className="span-details">Far Eastern University - Makati</span></h4>
                                        </Col>
                                    </Row>
                                </Col>
                                <Col className="col-12">
                                    <Row>
                                        <Col className="col-12">
                                            <h4 className="mt-3 ms-3">
                                                <span className="span-details">Hello, I'm Rae Dan. You can call me by my nickname Dan or Rae</span>
                                            </h4>
                                            <h4 className="mt-3 ms-3">
                                                <span className="span-details">I am a Bachelor of Information Technology graduate from Far Eastern University Makati. </span>
                                            </h4>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                        <Col className="col-12 mb-5">
                            <Row>
                                <Col className="col-12">
                                    <h5 className="mt-3 ms-3 text-center"><span className="full-stack-color">&gt; </span>So. A little info about me. <span className="full-stack-color">&lt; </span></h5>
                                </Col>
                                <Col className="col-12">
                                    <h5 className="mt-3 ms-3 text-center"><span className="full-stack-color">&gt; </span>I am aiming to become profecient in web developing. <span className="full-stack-color">&lt; </span></h5>
                                </Col>
                                <Col className="col-12">
                                    <h5 className="mt-3 ms-3 text-center"><span className="full-stack-color">&gt; </span>I have worked on projects using HTML, CSS, Javascript, REACT JS, NODE JS, and bootstrap. <span className="full-stack-color">&lt; </span></h5>
                                </Col>
                                <Col className="col-12">
                                    <h5 className="mt-3 ms-3 text-center"><span className="full-stack-color">&gt; </span>I have also worked on projects using PHP and JAVA. But I want to make REACT JS as my main language. <span className="full-stack-color">&lt; </span></h5>
                                </Col>
                                <Col className="col-12">
                                    <h5 className="mt-3 ms-3 text-center"><span className="full-stack-color">&gt; </span>I am interested in web developing because coding is something that I enjoy doing since I was child. <span className="full-stack-color">&lt; </span></h5>
                                </Col>
                                <Col className="col-12">
                                    <h5 className="mt-3 ms-3 text-center"><span className="full-stack-color">&gt; </span>I am always looking for new opportunities to improve my programming and web development skills. <span className="full-stack-color">&lt; </span></h5>
                                </Col>
                                <Col className="col-12">
                                    <h5 className="mt-3 ms-3 text-center"><span className="full-stack-color">&gt; </span>As much as I like programming, I also like drawing anime arts digitally  <span className="full-stack-color">&lt; </span></h5>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Container>
            </Container>
        </Fragment>
    )
}