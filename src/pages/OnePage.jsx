import { Fragment } from 'react'
import { Container } from 'react-bootstrap'
import { Fade } from "react-awesome-reveal"
//component
import Home from '../components/Home'
import About from '../components/About'
import Resume from '../components/Resume'
import Projects from '../components/Projects'
import Tools from '../components/Tools'
import Contact from '../components/Contact'

export default function OnePage() {

    return (
        <Fragment>
            <Container id="Home"></Container>
            <Fade>
                <Home />
            </Fade>
            <Container id="About" className="p-5"></Container>
            <Fade duration={500}>
                <About />
            </Fade>
            <Container id="Resume" className="p-5"></Container>
            <Fade duration={500}>
                <Resume />
            </Fade>
            <Container id="Projects" className="p-5"></Container>
            <Fade duration={500}>
                <Projects />
            </Fade>
            <Container id="Tools" className="p-5"></Container>
            <Fade duration={500}>
                <Tools />
            </Fade>
            <Container id="Contact" className="p-5"></Container>
            <Fade duration={500}>
                <Contact />
            </Fade>
        </Fragment>
    )
}
