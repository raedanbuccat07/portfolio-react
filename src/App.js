//dependencies
import { Fragment } from 'react'
import { Scrollbars } from 'react-custom-scrollbars-2'
import { BrowserRouter as Router, Switch } from 'react-router-dom'
//page
import OnePage from './pages/OnePage'
//component
import BGSlider from './components/BGSlider'
import AppNavbar from './components/AppNavbar'


function App() {

  return (
    <Fragment>
      <Scrollbars
        autoHide
        autoHideTimeout={1000}
        autoHideDuration={200}
        renderTrackVertical={({ style, ...props }) =>
          <div {...props} style={{ ...style, backgroundColor: 'black', right: '2px', bottom: '2px', top: '2px', borderRadius: '3px', width: '5px' }} />
        }
        renderThumbVertical={({ style, ...props }) =>
          <div {...props} style={{ ...style, width: '3px', borderRadius: '4px', boxShadow: '0 2px 4px 0 rgba(0, 0, 0, 0.16)', backgroundColor: '#be913d' }} />
        }>
        <Router>
          <Switch>
            <AppNavbar />
          </Switch>
        </Router>
        <OnePage />
      </Scrollbars>
      <BGSlider />
    </Fragment>

  );
}
export default App
